import WebSocket from 'ws'
import { default as express } from 'express'

const client = new WebSocket(process.env.HEARTRATE_URL)
let heartrateGlobal = 0;

client.on('message', (msg) => {
    const serialized = JSON.parse(msg.toString())
    heartrateGlobal = serialized.data.heartRate
    console.log(serialized.data.heartRate)
})

await new Promise(resolve => client.once('open', resolve))

const app = express()
app.get('/', (req, res) => {
    res.status(200).json({heartrate: heartrateGlobal})
})

app.listen(parseInt(process.env.SERVER_PORT), () => {
    console.log(`Listening on http://localhost:${process.env.SERVER_PORT}`)
})